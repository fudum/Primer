#include "stdafx.h"
#include "Chapter.h"
#include <fstream>
#include <iostream>

Chapter::Chapter(void)
{
}


Chapter::~Chapter(void)
{
}


/*****************************第六章*******************************/

//写入到文本文件中
void Chapter::chapter6_8_2()
{
	std::ofstream outFile;
	outFile.open("fish.txt");
	double wt = 125.8;
	outFile << wt << std::endl;
	outFile << "nihao" << std::endl;
	outFile.close();

	wt = 0;
	std::ifstream inFile;
	inFile.open("fish.txt");
	if (!inFile.is_open())
	{
		exit(EXIT_FAILURE);
	}
	inFile >> wt;
}


/*****************************第七章*******************************/

//递归调用
void Chapter::chapter7_16(int n)
{
	std::cout<<"Counting down ..." << n << "(n at " << &n << ")" <<std::endl;
	if (n > 0)
	{
		chapter7_16(n-1);
	}
	std::cout << n << ":Kaboom!" << " (n at" << &n << ")" <<std::endl;
}

/*
char ruler[Len];
int i;
for (i = 1; i < Len -2; i++)
{
ruler[i] = ' ';
}
ruler[Len - 1] = '\0';
int max = Len - 2;
int min = 0;
ruler[min] = ruler[max] = '|';
std::cout << ruler << std::endl;
Chapter c;
for (int i = 1; i < Divs; i++)
{
c.chapter7_17(ruler, min, max, i);
std::cout << ruler << std::endl;
for (int j = 1; j < Len - 2; j++)
{
ruler[j] = ' ';
}
}
*/
//打印尺子
void Chapter::chapter7_17(char ar[], int low, int high, int level)
{
	if (level == 0)
	{
		return;
	}
	int mid = (high + low) / 2;
	ar[mid] = '|';
	chapter7_17(ar, low, mid, level - 1);
	chapter7_17(ar, mid, high, level - 1);
}

//函数指针示例
double Chapter::betsy(int line)
{
	return 0.5 * line;
}

double Chapter::pam(int line)
{
	return 1.5 * line;
}
//函数指针
void Chapter::chapter7_18(int line, double(*p)(int))
{
	std::cout << line << " need " << p(line) << std::endl;
}